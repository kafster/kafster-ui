import Typography from "@mui/material/Typography";
import {Button, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const ViewListHeader = (props) => {
    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Views</Typography>
                <Typography variant={"body"}>0 Available</Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" startIcon={<AddIcon />}>
                    Add View
                </Button>
            </Grid>
        </Grid>
    )
}

const ViewListTable = (props) => {

    const rows =[{name:"first",calories:100,fat:100,protein:50,carbs:120}]
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Dessert (100g serving)</TableCell>
                        <TableCell align="right">Calories</TableCell>
                        <TableCell align="right">Fat&nbsp;(g)</TableCell>
                        <TableCell align="right">Carbs&nbsp;(g)</TableCell>
                        <TableCell align="right">Protein&nbsp;(g)</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow
                            key={row.name}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                            <TableCell align="right">{row.fat}</TableCell>
                            <TableCell align="right">{row.carbs}</TableCell>
                            <TableCell align="right">{row.protein}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

const Views = (props) => {
    return (
        <>
            <ViewListHeader {...props}/>
            <br/>
            <ViewListTable {...props}/>
        </>
    )
}

export default Views