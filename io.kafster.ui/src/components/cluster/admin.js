import {Button, Grid} from "@mui/material";
import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";

const ClusterAdmin = (props) => {
    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Clusters</Typography>
                <Typography variant={"body"}>0 Clusters, 0 Workers</Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" startIcon={<AddIcon />}>
                    Add Cluster
                </Button>
            </Grid>
        </Grid>
    )
}

export default ClusterAdmin