import axios from "axios";
import {Links} from "../util/links";

export const fetchSubscriptions = (session) => {
    return async () => {
        const links = new Links(session.links)
        return await axios.get(links.getHrefByRel("subscriptions")).then(r => r.data)
    }
}

export const fetchSubscription = (session) => {
    return async ({ queryKey }) => {
        const links = new Links(session.templates)
        const template = links.getHrefTemplate("subscription", {key: queryKey[1]})
        return await axios.get(template).then(r => r.data)
    }
}

export const createSubscription = (session) => {
    return async (subscription) => {
        const links = new Links(session.links)
        return await axios.post(links.getHrefByRel("subscriptions"), subscription).then(r => r.data)
    }
}