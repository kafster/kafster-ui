import React from "react";
import TextField from "@mui/material/TextField";
import {SimpleDialog} from "../../util/dialog";

const AddSubscriptionTitle = (props) => {
    return <span>Add Subscription</span>
}

const AddSubscriptionContent = (props) => {

    const {updateAddDialog} = props;
    
    const handleChange = (e) => {
        updateAddDialog(e.target.id, e.target.value)
    };

    return (
        <div>
            <TextField
                autoFocus
                required
                margin="dense"
                id="name"
                label="New Subscription Name"
                type="text"
                //error={name?false:true}
                fullWidth
                disabled={false}
                onChange={handleChange}
                variant="standard"
            />
            <TextField
                id="description"
                label="Description"
                margin="dense"
                multiline
                rows={4}
                //value={description}
                fullWidth
                onChange={handleChange}
                variant="standard"
            />
        </div>
    )
};

export const AddSubscriptionDialog = (props) => {

    const {open, hideDialog, canSubmit, createSubscription} = props;

    return (
        <SimpleDialog
            open={open}
            handleSubmit={createSubscription}
            hideDialog={hideDialog}
            canSubmit={canSubmit}
            content={<AddSubscriptionContent {...props}/>}
            title={<AddSubscriptionTitle {...props}/>}
            {...props}/>
    )
};
