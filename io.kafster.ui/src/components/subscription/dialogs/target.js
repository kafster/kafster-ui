import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleDown} from "@fortawesome/free-solid-svg-icons";
import {SimpleDialog} from "../../util/dialog";
import {JsonViewer} from "../../util/json";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Avatar, Button, Dialog, DialogActions, DialogContent, DialogTitle,
    IconButton,
    LinearProgress,
    List,
    ListItem,
    ListItemText
} from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from "@mui/material/Typography";
import {useMutation, useQuery} from "react-query";
import {fetchTargets, setSubscriptionTarget} from "./api";
import {
    ConfigureEndpointDialogContent,
    ConfigureEndpointDialogTitle,
    SelectEndpointDialogContent,
    SelectEndpointDialogTitle, SelectProviderDialogContent, SelectProviderDialogTitle
} from "./endpoint";

export const TargetConfigurationDialog = (props) => {

    const {subscription, provider, endpoint, open, hideDialog} = props;

    const targets = useQuery('targets', fetchTargets(subscription))
    const mutation = useMutation(setSubscriptionTarget(subscription), {
        onSuccess: hideDialog
    })

    const handleSubmit = () => {
        mutation.mutate(endpoint);
    };

    let title = <></>
    let content = <></>

    if (endpoint) {
        title = <ConfigureEndpointDialogTitle {...props}/>;
        content = <ConfigureEndpointDialogContent {...props}/>;
    }
    else if (provider) {
        title = <SelectEndpointDialogTitle {...props}/>;
        content = <SelectEndpointDialogContent {...props}/>;
    }
    else {
        title = <SelectProviderDialogTitle {...props}/>;
        content = <SelectProviderDialogContent {...props}/>;
    }

    return (
        <Dialog open={open} aria-labelledby="target-dialog-title" fullWidth={true} maxWidth={"sm"} hideBackdrop>
            <DialogTitle id="target-dialog-title">
                { title }
            </DialogTitle>
            <DialogContent dividers>
                { content }
            </DialogContent>
            <DialogActions>
                <Button onClick={hideDialog} color="primaryText" variant="text">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="secondary" variant="text" disabled={!endpoint}>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    )
};