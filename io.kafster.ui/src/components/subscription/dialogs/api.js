import axios from "axios";
import {Links} from "../../util/links";

export const fetchTargets = (subscription) => {
    return async () => {
        const links = new Links(subscription.links)
        return await axios.get(links.getHrefByRel("targets")).then(r => r.data)
    }
}

export const setSubscriptionTarget = (subscription) => {
    return async (endpoint) => {
        const links = new Links(subscription.links)
        return await axios.put(links.getHrefByRel("target"), endpoint).then(r => r.data)
    }
}

export const setSubscriptionSource = (subscription) => {
    return async (endpoint) => {
        const links = new Links(subscription.links)
        return await axios.put(links.getHrefByRel("source"), endpoint).then(r => r.data)
    }
}

export const setSubscriptionPipeline = (subscription) => {
    return async (pipeline) => {
        const links = new Links(subscription.links)
        return await axios.put(links.getHrefByRel("pipeline"), pipeline).then(r => r.data)
    }
}