import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, LinearProgress} from "@mui/material";
import {useMutation, useQuery} from "react-query";
import {
    ConfigureEndpointDialogContent,
    ConfigureEndpointDialogTitle,
    SelectEndpointDialogContent,
    SelectEndpointDialogTitle,
    SelectProviderDialogContent,
    SelectProviderDialogTitle
} from "./endpoint";
import {setSubscriptionSource} from "./api";
import {useSession} from "../../dashboard/session";
import {fetchProviders} from "../../endpoint/api";

export const SourceConfigurationDialog = (props) => {

    const {subscription, provider, endpoint, open, hideDialog} = props;

    const session = useSession()

    const mutation = useMutation(setSubscriptionSource(subscription), {
        onSuccess: hideDialog
    })

    const query = useQuery('providers', fetchProviders(session.data, {
        enabled:!!session && !!session.data
    }));

    if (session.isLoading || query.isLoading) {
        return <LinearProgress color="secondary"/>
    }

    const handleSubmit = () => {
        mutation.mutate(endpoint);
    }

    const providers = query.data

    let title = <></>
    let content = <></>

    if (endpoint) {
        title = <ConfigureEndpointDialogTitle providers={providers} {...props}/>;
        content = <ConfigureEndpointDialogContent providers={providers} {...props}/>;
    }
    else if (provider) {
        title = <SelectEndpointDialogTitle providers={providers} {...props}/>;
        content = <SelectEndpointDialogContent providers={providers} {...props}/>;
    }
    else {
        title = <SelectProviderDialogTitle providers={providers} {...props}/>;
        content = <SelectProviderDialogContent providers={providers} {...props}/>;
    }

    return (
        <Dialog open={open} aria-labelledby="source-dialog-title" fullWidth={true} maxWidth={"sm"} hideBackdrop>
            <DialogTitle id="source-dialog-title">
                { title }
            </DialogTitle>
            <DialogContent dividers>
                { content }
            </DialogContent>
            <DialogActions>
                <Button onClick={hideDialog} color="secondary" variant="text">
                    Cancel
                </Button>
                <Button onClick={handleSubmit}  variant="text" disabled={!endpoint}>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    )
};


