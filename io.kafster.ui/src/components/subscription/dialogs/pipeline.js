import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStream} from "@fortawesome/free-solid-svg-icons";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    LinearProgress,
    List,
    ListItem,
    ListItemText
} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {ErrorAlert} from "../../util/errors";
import {useMutation} from "react-query";
import {setSubscriptionPipeline, setSubscriptionSource} from "./api";

const SelectPipelineTitle = ({classes}) => {
    return (
        <div className={classes.avatar}>
            <Avatar aria-label="pipeline">
                <FontAwesomeIcon icon={faStream} size="sm"/>
            </Avatar>
            <span style={{"marginLeft":"8px","marginTop":"12px"}}>Select a Pipeline</span>
        </div>
    )
};

const SelectPipelineContent = (props) => {

    const {classes, pipeline, pipelines, loadPipelines, showPipelineDialog } = props;

    if (pipelines.fetch.error) {
        return <ErrorAlert message={pipeline.error} open={true}/>
    }

    if (pipelines.fetch.active) {
        return <LinearProgress classes={classes} color="secondary"/>
    }

    if (pipelines.list == null) {
        loadPipelines();
        return <LinearProgress classes={classes} color="secondary"/>
    }

    const PipelineListItem = ({item}) => {
        return (
            <ListItem button onClick={e=>showPipelineDialog(item)} disableRipple>
                <ListItemText primary={item.name} secondary={item.description}/>
            </ListItem>
        )
    };

    const NewPipelineListItem = () => {
        return (
            <ListItem button onClick={e=>showPipelineDialog()} disableRipple>
                <ListItemText primary="Add a Pipeline" secondary="Create a new Pipeline for this Subscription."/>
            </ListItem>
        )
    };

    const list = pipelines.list || [];
    return (
        <List>
            { list.map(p => <PipelineListItem item={p} {...props}/>)}
            <NewPipelineListItem {...props}/>
        </List>
    )
};

const ConfigurePipelineTitle = (props) => {
    const {showPipelineDialog, pipeline} = props;
    return (
        <div>
            <IconButton aria-label="back" onClick={(e=>showPipelineDialog(null))}>
                <ArrowBackIcon />
            </IconButton>
            <span style={{"marginLeft":"8px","marginTop":"12px"}}>Configure Pipeline </span>
        </div>
    )
};

const ConfigurePipelineContent = (props) => {

    const {classes, pipeline} = props;
    const selected = pipeline.dialog.selected;

    return (
        <ListItem>
            <ListItemText primary={selected.name} secondary={selected.description}/>
        </ListItem>
    )
};

export const PipelineConfigurationDialog = (props) => {

    const {pipeline, open, subscription, hideDialog} = props;

    let title = <></>
    let content = <></>

    if (pipeline != null) {
        title = <ConfigurePipelineTitle {...props}/>;
        content = <ConfigurePipelineContent {...props}/>;
    }
    else {
        title = <SelectPipelineTitle {...props}/>;
        content = <SelectPipelineContent {...props}/>;
    }

    const mutation = useMutation(setSubscriptionPipeline(subscription), {
        onSuccess: hideDialog
    })

    const handleSubmit = () => {
        mutation.mutate(pipeline)
    };

    return (
        <Dialog open={open} aria-labelledby="pipeline-dialog-title" fullWidth={true} hideBackdrop >
            <DialogTitle id="pipeline-dialog-title">
                { title }
            </DialogTitle>
            <DialogContent dividers>
                { content }
            </DialogContent>
            <DialogActions>
                <Button onClick={hideDialog} color="primaryText" variant="text">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="secondary" variant="text" disabled={!pipeline}>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    )
};


