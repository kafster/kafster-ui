import React from "react";
import FormDialog from "../../util/form";

const schema = {
    type:"object",
    properties:{
        "name":{
            type:"string",
            title:"Subscription Name"
        },
        "description":{
            type:"string",
            title:"Description"
        },
        "retention":{
            type:"integer",
            title:"Retention Time (mS)"
        },
        "workers":{
            type:"integer",
            title:"Worker Threads",
            enum:[1,2,4,8]
        }
    },
    required:["name"]
}

const options = {
    name: {
        "ui:autofocus": true
    },
    description:{
        "ui:widget": "textarea",
        "ui:options": {
            rows: 4
        }
    }
}

const Title = (props) => {
    return <span>Subscription Settings</span>
}

export const SubscriptionSettingsDialog = (props) => {

    const {subscription, update, updateSettings, handleClose} = props;

    const handleSubmit = (values) => {
        updateSettings(values)
    };

    const initialValues = {
        name:subscription.name,
        description:subscription.description
    }

    return (
        <FormDialog
            open={update.dialog.active}
            onSubmit={handleSubmit}
            hideDialog={handleClose}
            canSubmit={() => true}
            title={<Title {...props}/>}
            schema={schema}
            options={options}
            initialValues={initialValues}
            {...props}/>
    )
};