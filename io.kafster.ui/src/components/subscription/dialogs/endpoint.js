import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Avatar,
    LinearProgress,
    List,
    ListItem,
    ListItemText, Stack
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Typography from "@mui/material/Typography";
import {JsonViewer} from "../../util/json";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleDown} from "@fortawesome/free-solid-svg-icons";
import {ErrorAlert} from "../../util/errors";
import TreeComponent from "../../util/tree";

export const SelectProviderDialogTitle = () => {
    return (
        <Stack direction={"row"} spacing={4}>
            <Avatar>
                <FontAwesomeIcon icon={faAngleDoubleDown} size="sm"/>
            </Avatar>
            <span style={{"marginLeft":"12px","marginTop":"4px"}}>Select a Provider</span>
        </Stack>
    )
};

export const SelectProviderDialogContent = (props) => {

    const {providers, setProvider } = props;

    if (!providers) {
        return <Typography>No providers defined</Typography>
    }

    return (
        <TreeComponent roots={providers}
                       getNodeLabel = {provider => provider.presentationName }
                       getNodeId = {provider => provider.key }
                       getChildren = {provider => provider.children}
                       handleSelect = {provider => setProvider(provider)}/>
    )
};

export const SelectEndpointDialogTitle = (props) => {
    const {setProvider} = props;
    return (
        <span>
            <IconButton aria-label="back" onClick={e=>setProvider(null)}>
                <ArrowBackIcon />
            </IconButton>
            <span style={{"marginLeft":"8px","marginTop":"12px"}}>Select an Endpoint</span>
        </span>
    )
};

export const SelectEndpointDialogContent = (props) => {

    const {provider, setEndpoint} = props;

    const EndpointListItem = (props) => {
        const {endpoint} = props;
        return (
            <ListItem button onClick={e=>setEndpoint(endpoint)} disableRipple>
                <ListItemText primary={endpoint.metadata.name} secondary={endpoint.metadata.description}/>
            </ListItem>
        )
    };

    return (
        <List>
            { provider.endpoints && provider.endpoints.map(endpoint => <EndpointListItem endpoint={endpoint} {...props}/>)}
        </List>
    )
};

export const ConfigureEndpointDialogTitle = (props) => {
    const {setEndpoint} = props;
    return (
        <span>
            <IconButton aria-label="back" onClick={(e=>setEndpoint(null))}>
                <ArrowBackIcon />
            </IconButton>
            <span style={{"marginLeft":"8px","marginTop":'12px"'}}>Configure Endpoint</span>
        </span>
    )
};

export const ConfigureEndpointDialogContent = (props) => {

    const {endpoint} = props;

    let name = endpoint.name;
    let description = endpoint.description;

    if (endpoint.metadata != null) {
        name = endpoint.metadata.name;
        description = endpoint.metadata.description;
    }

    return (
        <div>
            <ListItem>
                <ListItemText primary={name} secondary={description}/>
            </ListItem>
            <ListItem>
                <ListItemText secondary="This endpoint does not require any additional configuration"/>
            </ListItem>
            <ListItem>
                <Accordion variant={"outlined"} elevation={0}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="schema-content"
                        id="schema-header">
                        <Typography>Schema</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        {
                            endpoint.metadata &&
                            endpoint.metadata.contentModel &&
                            endpoint.metadata.contentModel.schema &&
                            <JsonViewer data={endpoint.metadata.contentModel.schema}/>
                        }
                    </AccordionDetails>
                </Accordion>
            </ListItem>
        </div>
    )
}