import {
    Avatar,
    Button,
    Card,
    CardHeader,
    Fab,
    Grid,
    LinearProgress,
    List,
    ListItem,
    ListItemAvatar, ListItemText
} from "@mui/material";
import Typography from "@mui/material/Typography";
import AddIcon from '@mui/icons-material/Add';
import React, {useState} from "react";
import {AddSubscriptionDialog} from "./dialogs/add";
import IconButton from "@mui/material/IconButton";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import {Link} from "react-router-dom";
import WorkspacesIcon from "@mui/icons-material/Workspaces";
import {TimestampMillis} from "../util/time";
import {useMutation, useQuery} from "react-query";
import {createSubscription, fetchSubscriptions} from "./api";
import {useSession} from "../dashboard/session";
import {SimpleProgress} from "../util/progress";
import {KafkaLogo} from "../util/icon";
import Divider from "@mui/material/Divider";

const SubscriptionListHeader = (props) => {
    const {showDialog} = props

    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Subscriptions</Typography>
                <Typography variant={"body"}>0 Active, 0 Inactive</Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" startIcon={<AddIcon />} onClick={showDialog}>
                    Add Subscription
                </Button>
            </Grid>
        </Grid>
    )
}

const SubscriptionGridItem = ({subscription}) => {

    let subheader = subscription.description;
    if (subheader == null) {
        subheader = (
            <>
                <span>Updated </span>
                <TimestampMillis timestamp={subscription.modified}/>
            </>
        )
    }

    return (
        <Grid item xl={4} sm={6} xs={12}>
            <Card raised={false}>
                <CardHeader
                    avatar={
                        <Avatar aria-label="subscription">
                            <WorkspacesIcon/>
                        </Avatar>
                    }
                    action={
                        <IconButton aria-label="detail" component={Link} to={"/subscriptions/"+subscription.key}>
                            <NavigateNextIcon />
                        </IconButton>
                    }
                    title={subscription.name}
                    subheader={subheader}
                />
            </Card>
        </Grid>
    )
};

const SubscriptionGrid = ({subscriptions}) => {
    return (
        <Grid container justifyContent="flex-start"  spacing={4}>
            {
                subscriptions &&
                subscriptions
                    .sort((a,b) => a.name.localeCompare(b.name))
                    .map(subscription => <SubscriptionGridItem subscription={subscription} />)
            }
        </Grid>
    )
}

const SubscriptionListItem = ({subscription, history}) => {

    let header = (
            <Typography>{subscription.name}</Typography>
    )

    let subheader = subscription.description;
    if (subheader == null) {
        subheader = (
            <>
                <span>Updated </span>
                <TimestampMillis timestamp={subscription.modified}/>
            </>
        )
    }

    return (
        <>
            <ListItem
                button onClick={() => history.push("/subscriptions/" + subscription.key)} disableRipple
                secondaryAction={
                    <IconButton aria-label="detail" component={Link} to={"/subscriptions/"+subscription.key}>
                        <NavigateNextIcon />
                    </IconButton>
                }>
                <ListItemAvatar>
                    <Avatar>
                        <KafkaLogo/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={header} secondary={subheader} />
            </ListItem>
            <Divider/>
        </>
    )
}

const SubscriptionList = (props) => {
    const {subscriptions} = props
    return (
        <Grid container spacing={1}>
            <List sx={{"width":"100%"}}>
                {
                    subscriptions &&
                    subscriptions
                        .sort((a,b) => a.name.localeCompare(b.name))
                        .map(subscription => <SubscriptionListItem subscription={subscription} {...props} />)
                }
            </List>
        </Grid>
    )
}

const Subscriptions = (props) => {

    const [dialog, setDialog] = useState({open:false,values:{}})

    const updateDialog = (field, value) => {
        const values = dialog.values
        values[field] = value
        setDialog({...dialog, values:values})
    }

    const session = useSession()
    const mutation = useMutation(createSubscription(session.data), {
        onSuccess: (data) => {
            props.history.push("/subscriptions/" + data.key);
        }
    })

    const doSubmit = ()  => {
        mutation.mutate(dialog.values)
    }

    const subscriptions = useQuery('subscriptions', fetchSubscriptions(session.data));

    return (
        <>
            <SimpleProgress open={session.isLoading || subscriptions.isLoading}/>
            <SubscriptionListHeader showDialog={() => setDialog({...dialog, open:true})} {...props}/>
            { subscriptions.isError && <div/>  }
            { subscriptions.isSuccess &&  <SubscriptionList subscriptions={subscriptions.data.subscriptions} {...props}/> }
            <AddSubscriptionDialog
                open={dialog.open}
                hideDialog={() => setDialog({...dialog, open:false})}
                canSubmit={() => dialog.values["name"] || false}
                createSubscription={doSubmit}
                updateAddDialog={updateDialog}
                values={dialog.values}
                {...props}/>
        </>
    )
}

export default Subscriptions