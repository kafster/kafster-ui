import React, {useState} from "react"
import {Link} from "react-router-dom"
import {TimestampMillis} from "../util/time"
import {SourceConfigurationDialog} from './dialogs/source';
import {TargetConfigurationDialog} from './dialogs/target';
import {PipelineConfigurationDialog} from './dialogs/pipeline';
import {SubscriptionSettingsDialog} from "./dialogs/settings";
import {
    Accordion, AccordionDetails, AccordionSummary,
    Card, CardContent,
    CardHeader, LinearProgress,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    Menu,
    MenuItem
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import List from "@mui/material/List";
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import BarChartIcon from '@mui/icons-material/BarChart';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleDown, faStream, faDatabase} from "@fortawesome/free-solid-svg-icons";
import {useQuery} from "react-query";
import {fetchSubscription, fetchSubscriptions} from "./api";
import {useSession} from "../dashboard/session";

const STATES = {
    "CREATED": () => {
        return {
            message: "Created",
            secondary: "Inactive",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" disabled>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        };
    },
    "DISABLED":() => {
        return {
            message: "Disabled",
            secondary: "",
            icon: <FiberManualRecordIcon color="disabled"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" disabled>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "QUEUED":({handlePause}) => {
        return {
            message: "Queued for Execution",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" onClick={handlePause}>
                    <PauseIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "AVAILABLE":({handlePlay}) => {
        return {
            message: "Waiting to Execute",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" onClick={handlePlay}>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "REBALANCING":() => {
        return {
            message: "Re-balancing",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" disabled>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "RUNNING":({handlePause}) => {
        return {
            message: "Running",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" onClick={handlePause}>
                    <PauseIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "PENDING_SHUTDOWN":() => {
        return {
            message: "Shutting Down",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" disabled>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "ERROR":({handlePause}) => {
        return {
            message: "Error",
            secondary: "",
            icon: <FiberManualRecordIcon color="error"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" onClick={handlePause}>
                    <PauseIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "NOT_RUNNING":({handlePlay}) => {
        return {
            message: "Not Running",
            secondary: "",
            icon: <FiberManualRecordIcon color="primary"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" onClick={handlePlay}>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    },
    "UNKNOWN":() => {
        return {
            message: "Unknown",
            secondary: "",
            icon: <FiberManualRecordIcon color="disabled"/>,
            buttons: [
                <IconButton aria-label="rewind" disabled>
                    <SkipPreviousIcon/>
                </IconButton>,
                <IconButton aria-label="play/pause" disabled>
                    <PlayArrowIcon style={{height: 32, width: 32}}/>
                </IconButton>
            ]
        }
    }
};

const UndefinedSource = (props) => {
    const {subscription, dialog, toggleDialog, hideDialog} = props;

    return (
        <div>
            <SourceConfigurationDialog open={dialog.open} handleClose={hideDialog} {...props}/>
            <ListItem button  onClick={e => toggleDialog(e)} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <Typography variant="h5">1</Typography>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Select a source endpoint" secondary="Click here to configure a data source for your subscription"/>
            </ListItem>
        </div>
    )
};

const SourceListItem = (props) => {

    const [dialog, setDialog] = useState({open:false, provider:null, endpoint:null})
    const hideDialog = () => {
        setDialog({...dialog, open:false})
    }

    const toggleDialog = () => {
        setDialog({...dialog, open:!dialog.open})
    }

    const setEndpoint = (endpoint) => {
        setDialog({...dialog, endpoint:endpoint})
    }

    const setProvider = (provider) => {
        setDialog({...dialog, provider:provider})
    }

    const {subscription} = props;

    if (subscription.source == null) {
        return <UndefinedSource dialog={dialog} toggleDialog={toggleDialog} hideDialog={hideDialog} {...props}/>
    }

    const uri = subscription.source.uri;
    const secondary = subscription.source.name || uri;

    return (
        <div>
            <SourceConfigurationDialog
                open={dialog.open}
                hideDialog={hideDialog}
                provider={dialog.provider}
                endpoint={dialog.endpoint}
                setProvider={setProvider}
                setEndpoint={setEndpoint}
                {...props}/>

            <ListItem button onClick={toggleDialog} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <FontAwesomeIcon icon={faAngleDoubleDown} size="sm"/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={"Stream"} secondary={secondary} />
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="detail" component={Link} to={"/endpoints/" + subscription.source.key}>
                        <NavigateNextIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    )
};

const UndefinedPipeline = (props) => {

    const {subscription, dialog, toggleDialog, closeDialog} = props;
    const disabled = subscription.source == null;

    return (
        <div>
            <PipelineConfigurationDialog open={dialog.active} handleClose={closeDialog} {...props}/>
            <ListItem button onClick={e=>toggleDialog(e)} disabled={disabled} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <Typography variant="h5">2</Typography>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Select a pipeline"  secondary="Click here to configure a pipeline for your subscription"/>
            </ListItem>
        </div>
    )
};

const PipelineListItem = (props) => {

    const {subscription} = props;
    const [dialog,setDialog] =  useState({open:false,selected:null})

    const toggleDialog = (e) => {
        setDialog({...dialog, active:!dialog.open})
    };

    const closeDialog = (e) => {
        setDialog({...dialog, open:false})
    };

    const setPipeline = (pipeline) => {
        setDialog({selected:pipeline, ...dialog})
    }

    if (subscription.pipeline == null) {
        return <UndefinedPipeline dialog={dialog} toggleDialog={toggleDialog} closeDialog={closeDialog} {...props}/>
    }

    return (
        <div>
            <PipelineConfigurationDialog open={dialog.open} handleClose={closeDialog} pipeline={dialog.selected} setPipeline={setPipeline} {...props}/>
            <ListItem button onClick={e=>toggleDialog(e)} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <FontAwesomeIcon icon={faStream} size="sm"/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Pipeline" secondary={subscription.pipeline.name}/>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="detail" component={Link} to={"/pipelines/"+subscription.pipeline.key}>
                        <NavigateNextIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    )
};

const UndefinedTarget = (props) => {
    const {subscription, dialog, toggleDialog, hideDialog} = props;
    const disabled = subscription.pipeline == null;

    return (
        <div>
            <TargetConfigurationDialog open={dialog.open} {...props}/>
            <ListItem button onClick={toggleDialog} handleClose={hideDialog} disabled={disabled} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <Typography variant="h5">3</Typography>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Select a target endpoint"  secondary="Click here to configure a data destination for your subscription"/>
            </ListItem>
        </div>
    )
};

const TargetListItem = (props) => {

    const [dialog, setDialog] = useState({open:false, provider:null, endpoint:null})
    const hideDialog = () => {
        setDialog({...dialog, open:false})
    }

    const toggleDialog = () => {
        setDialog({...dialog, open:!dialog.open})
    }

    const setEndpoint = (endpoint) => {
        setDialog({...dialog, endpoint:endpoint})
    }

    const setProvider = (provider) => {
        setDialog({...dialog, provider:provider})
    }

    const {subscription} = props;

    if (subscription.target == null) {
        return <UndefinedTarget dialog={dialog} toggleDialog={toggleDialog} hideDialog={hideDialog} {...props}/>
    }

    const uri = subscription.target.uri;
    const secondary = subscription.target.name || uri;

    return (
        <div>
            <TargetConfigurationDialog
                open={dialog.open}
                hideDialog={hideDialog}
                provider={dialog.provider}
                endpoint={dialog.endpoint}
                setProvider={setProvider}
                setTarget={setEndpoint}
                {...props}/>

            <ListItem button onClick={toggleDialog} disableRipple>
                <ListItemAvatar>
                    <Avatar>
                        <FontAwesomeIcon icon={faAngleDoubleDown} size="sm"/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={"Stream"} secondary={secondary} />
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="detail" component={Link} to={"/endpoints/" + subscription.target.key}>
                        <NavigateNextIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    )
};

const SubscriptionMenu = (props) => {

    const {menu, hideMenu} = props;

    const handleDelete = (e) => {
        e.stopPropagation();
    };

    const handleSettings = (e) => {
        e.stopPropagation();
    };

    const handleHideMenu = (e) => {
        e.stopPropagation();
        hideMenu()
    };

    return (
        <span>
            <Menu
                id="subscription-menu"
                anchorEl={menu.anchor}
                keepMounted
                open={menu.open}
                onClose={handleHideMenu}>
                <MenuItem onClick={handleSettings}>Settings</MenuItem>
                <Divider />
                <MenuItem onClick={handleDelete}>Delete Subscription</MenuItem>
            </Menu>
        </span>
    )
};

const SubscriptionHeader = (props) => {

    const [menu, setMenu] = useState({
        open: false,
        anchor: null
    })

    const hideMenu = () => {
        setMenu({open:false,anchor:null})
    }

    const showMenu = (anchor) => {
        setMenu({open:true,anchor:anchor})
    }

    const {subscription, history} = props;

    return (
        <CardHeader
            avatar={
                <IconButton aria-label="detail" onClick={e=>history.goBack()}>
                    <ArrowBackIcon />
                </IconButton>
            }
            action={
                <span>
                    <IconButton aria-label="views" component={Link} to={"/subscriptions/"+subscription.key+"/views"} title="Views">
                        <BarChartIcon/>
                    </IconButton>
                    <IconButton aria-label="more" onClick={e => showMenu(e.currentTarget)}>
                        <MoreVertIcon />
                    </IconButton>
                    <SubscriptionMenu menu={menu} hideMenu={hideMenu} {...props}/>
                </span>
            }
            title={<Typography variant="h4" color="textPrimary">subscriptions / {subscription.name}</Typography>}
            subheader={
                <span>
                    <span>Updated </span>
                    <TimestampMillis timestamp={subscription.modified}/>
                </span>
            }/>
    )
};

const SubscriptionDetails = (props) => {

    const {subscription} = props;
    return (
        <Card raised={false}>
            <SubscriptionHeader {...props}/>
            <CardContent>
                <Typography variant="body1" component="p">
                    {subscription.description}
                </Typography>
                <List>
                    <SourceListItem {...props}/>
                    <PipelineListItem {...props}/>
                    <TargetListItem {...props}/>
                </List>
            </CardContent>
        </Card>
    )
};

const SubscriptionActivity = (props) => {

    const [expanded, setExpanded] = useState(false)

    const togglePanel = () => {
        setExpanded(!expanded)
    }

    const {subscription,  handlePlay, handlePause} = props;
    const factory = (subscription.status && subscription.status.state) ? STATES[subscription.status.state] : STATES["UNKNOWN"];
    const state = factory ? factory({handlePlay, handlePause}) : null;

    return (
        <Accordion expanded={expanded} timeout="auto" unmountOnExit style={{"width":"100%"}}>
            <AccordionSummary expandIcon={<ExpandMoreIcon/>} onClick={() => togglePanel()}>
                <FiberManualRecordIcon color="primary" style={{marginRight:"8px"}}/>
                <Typography>{subscription.name} is {state.message}</Typography>
                {state.secondary && <Typography style={{marginLeft:"4px"}}>and {state.secondary}</Typography>}
            </AccordionSummary>
            <AccordionDetails>
                <Typography>No Workers</Typography>
            </AccordionDetails>
            <Divider/>
            <>
                {state.buttons}
            </>
        </Accordion>
    )
};

const Subscription = (props) => {

    const session = useSession();
    const subscription = useQuery(['subscriptions', props.match.params.key], fetchSubscription(session.data), {
        enabled: !!session.data
    });

    if (session.isLoading || subscription.isLoading) {
        return <LinearProgress color="secondary"/>
    }

    return (
        <div>
            <SubscriptionDetails subscription={subscription.data} {...props}/>
            <SubscriptionActivity subscription={subscription.data} {...props}/>
        </div>
    )
};

export default Subscription
