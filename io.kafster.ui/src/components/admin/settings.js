import IconButton from "@mui/material/IconButton";
import {Menu,MenuItem} from "@mui/material";
import SettingsIcon from '@mui/icons-material/Settings';
import {useState} from "react";

const Settings = () => {

    const [open, setOpen] = useState(false);
    const [anchor, setAnchor] = useState(null);

    const handleShowMenu = (e) => {
        setAnchor(e.currentTarget)
        setOpen(true)
    };

    const handleCloseMenu = (e) => {
        setOpen(false)
    };

    return (
        <>
            <IconButton
                aria-label="current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={e => handleShowMenu(e)}
                color="inherit">
                <SettingsIcon fontSize={"medium"}/>
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={anchor}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onClose={handleCloseMenu}
            >
                <MenuItem onClick={handleCloseMenu}>Profile</MenuItem>
                <MenuItem onClick={handleCloseMenu}>Sign Out</MenuItem>
            </Menu>
        </>
    )
};

export default Settings