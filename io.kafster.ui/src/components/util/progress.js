import {LinearProgress} from "@mui/material";
import React from "react";

export const SimpleProgress = ({open}) => {
    return open && <LinearProgress color="secondary"/>
}