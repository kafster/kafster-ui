import React from 'react';

import {utils} from '@rjsf/core';
import AddButton from "@rjsf/core/lib/components/AddButton";
import {Grid} from "@mui/material";

const { canExpand } = utils;

const ObjectFieldTemplate = ({
         DescriptionField,
         description,
         TitleField,
         title,
         properties,
         required,
         disabled,
         readonly,
         uiSchema,
         idSchema,
         schema,
         formData,
         onAddClick,
     }) => {

    return (
        <div>
            {(uiSchema['ui:title']) && (
                <TitleField
                    id={`${idSchema.$id}-title`}
                    title={title}
                    required={required}
                />
            )}
            {description && (
                <DescriptionField
                    id={`${idSchema.$id}-description`}
                    description={description}
                />
            )}
            <Grid container={true} spacing={2}>
                {properties.map((element, index) => (
                    <Grid
                        item={true}
                        xs={12}
                        key={index}
                        style={{ marginBottom: '10px' }}
                    >
                        {element.content}
                    </Grid>
                ))}
                {canExpand(schema, uiSchema, formData) && (
                    <Grid container justify='flex-end'>
                        <Grid item={true}>
                            <AddButton
                                className='object-property-expand'
                                onClick={onAddClick(schema)}
                                disabled={disabled || readonly}
                            />
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </div>
    );
};

export default ObjectFieldTemplate;