import React from 'react';
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';
import {Alert, Snackbar} from '@mui/material';

export const ErrorAlert = ({message,open,handleClose}) => {
    return (
        <Snackbar
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            anchorOrigin={{vertical:'top', horizontal:'center' }}
            action={
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
            }>
            <Alert onClose={handleClose} severity="error">
                {message}
            </Alert>
        </Snackbar>
    )
};