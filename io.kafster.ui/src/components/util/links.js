import React from "react"

export class Links {
    constructor(links=[]) {
        this.links = links
    }
    getByRel(rel) {
        return this.links && this.links.find(link => link.rel === rel)
    }

    getHrefByRel(rel) {
        const link = this.getByRel(rel);
        if (link)
            return link.href;
        return null
    }
    getHrefTemplate(rel, values) {
        let tpl = unescape(this.getHrefByRel(rel));
        return tpl.replace(/{([^}]+)?}/g, ($1, $2) => { return values[$2] })
    }
}


