import React from "react"
import Moment from 'react-moment'

export const TimestampMillis = ({timestamp}) => {
    const iso = new Date(timestamp).toISOString()
    return (
        <span><Moment>{iso}</Moment></span>
    )
}