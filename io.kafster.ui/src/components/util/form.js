import React from "react";
import {SimpleDialog} from "./dialog";
import ObjectFieldTemplate from "./rjsf";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Form from "@rjsf/core";

const TitleField = ({title}) => {
    return (
        <Box mb={0} mt={0}>
            <Typography variant="h5">{title}</Typography>
        </Box>
    )
}

class FormDialog extends React.Component {

    submit() {
        if (this.form) {
            this.form.submit();
        }
    }

    setForm(form) {
        this.form = form;
    }

    render() {

        const fields = {
            TitleField:TitleField
        }

        const props = {
            ...this.props,
            onSubmit: (form) => this.props.onSubmit(form.formData),
        }

        return (
            <SimpleDialog
                handleSubmit={() => this.submit()}
                content={
                    <Form
                        uiSchema={this.props.options}
                        formData={this.props.initialValues}
                        fields={fields}
                        ObjectFieldTemplate={ObjectFieldTemplate}
                        ref={(form) => {this.setForm(form)}}
                        {...props}>
                        <div>
                            <button type="submit" style={{display:"none"}}></button>
                        </div>
                    </Form>
                }
                {...this.props}/>
        );
    }
}

export default FormDialog;