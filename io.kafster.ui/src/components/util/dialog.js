import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@mui/material";

export const SimpleDialog = (props) => {
    const {title, content, open, hideDialog, handleSubmit, canSubmit} = props;
    return (
        <Dialog open={open} aria-labelledby="simple-dialog-title" fullWidth={true} hideBackdrop>
            <DialogTitle id="simple-dialog-title">
                { title }
            </DialogTitle>
            <DialogContent dividers>
                { content }
            </DialogContent>
            <DialogActions>
                <Button onClick={hideDialog} variant="text" color="secondary">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} variant="text" disabled={!canSubmit()}>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    )
};