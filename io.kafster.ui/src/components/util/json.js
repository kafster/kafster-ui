import ReactJson from 'react-json-view'
import React from "react";

export const JsonViewer= ({data}) => {
    return <ReactJson src={data}
                      iconStyle={"square"}
                      enableClipboard={false}
                      displayDataTypes={false}
                      displayObjectSize={false}
                      name={false}
                      style={{"fontSize":"12px"}}/>
};

