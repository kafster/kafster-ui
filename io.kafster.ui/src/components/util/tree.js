import React from 'react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';

const renderTree = (node, props) => {
    const {getChildren, getNodeId, getNodeLabel, handleSelect} = props;
    const children = getChildren(node);
    return (
        <TreeItem nodeId={getNodeId(node)} label={getNodeLabel(node)} onLabelClick={e => handleSelect(node)}>
            { children ? children.map(child => renderTree(child, props)) : <TreeItem/>}
        </TreeItem>
    );
};

const TreeComponent = (props) => {

    const {roots} = props;
    return (
        <TreeView defaultCollapseIcon={<ExpandMoreIcon />} defaultExpandIcon={<ChevronRightIcon />} {...props}>
            {roots && roots.map(root => renderTree(root, props))}
        </TreeView>
    );
};

export default TreeComponent


