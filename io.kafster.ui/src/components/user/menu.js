import IconButton from "@mui/material/IconButton";
import {ListItemIcon, ListItemText, Menu, MenuItem} from "@mui/material";
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import {useState} from "react";
import Divider from "@mui/material/Divider";
import NameAvatar from "../util/avatar";

const UserMenu = () => {

    const [menu, setMenu] = useState({open:false, anchor:null});

    const showMenu = (e) => {
        setMenu({open:true, anchor:e.currentTarget})
    };

    const closeMenu = (e) => {
        setMenu({open:false, anchor:null})
    };

    return (
        <>
            <IconButton
                aria-label="current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={e => showMenu(e)}
                color="inherit">
                <AccountCircleIcon fontSize={"large"}/>
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={menu.anchor}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={menu.open}
                onClose={closeMenu}
            >
                <MenuItem onClick={closeMenu}>
                    <ListItemIcon><PersonOutlineOutlinedIcon fontSize={"small"}/></ListItemIcon>
                    <ListItemText>Profile</ListItemText>
                </MenuItem>
                <MenuItem onClick={closeMenu}>
                    <ListItemIcon><SettingsOutlinedIcon fontSize={"small"}/></ListItemIcon>
                    <ListItemText>Settings</ListItemText>
                </MenuItem>
                <Divider />
                <MenuItem onClick={closeMenu}>
                    <ListItemIcon><LogoutOutlinedIcon fontSize={"small"}/></ListItemIcon>
                    <ListItemText>Sign Out</ListItemText>
                </MenuItem>
            </Menu>
        </>
    )
};

export default UserMenu