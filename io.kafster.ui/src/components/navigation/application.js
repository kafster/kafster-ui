import MiniDrawer from "./drawer";
import {useSession} from "../dashboard/session";

const Application = (props) => {
    const session = useSession()
    return <MiniDrawer session={session} {...props}/>
}

export default Application