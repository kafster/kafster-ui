import * as React from "react";
import {Route as ReactRoute} from "react-router";

export const Route = (props) => {
    return <ReactRoute exact {...props}/>
}

export const DefaultRoute = (props) => {
    return <Route {...props} />
}