import Typography from "@mui/material/Typography";
import {
    Avatar,
    Button,
    Grid,
    LinearProgress,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Menu,
    MenuItem
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import {styled} from "@mui/material/styles";
import {fetchProvider, fetchProviders, importAsyncAPI, uploadAsyncAPI} from "./api";
import {useMutation, useQuery} from "react-query";
import {useSession} from "../dashboard/session";
import React, {useState} from "react";
import {AddEndpointsDialog} from "./dialogs/add";
import WorkspacesIcon from "@mui/icons-material/Workspaces";
import IconButton from "@mui/material/IconButton";
import {Link} from "react-router-dom";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Box from "@mui/material/Box";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import Divider from "@mui/material/Divider";
import {SimpleProgress} from "../util/progress";

const Input = styled('input')({
    display: 'none',
});

const EndpointProviderHeader = (props) => {

    const {provider, showMenu} = props

    return (
        <Grid container justifyContent="flex-start" spacing={1}>
            <Grid item>
                <Typography variant={"h4"}>Endpoints / {provider.presentationName}</Typography>
                <Typography variant={"body"}>{provider.data ? provider.data.endpoints.length : 0} Endpoints</Typography>
            </Grid>
            <Grid item>
                <IconButton aria-label="more" onClick={e => showMenu(e.currentTarget)}>
                    <MoreVertIcon />
                </IconButton>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="subheader" component="p">
                    {provider.data ? provider.data.description : ""}
                </Typography>
            </Grid>
        </Grid>
    )
}

const EndpointListItem = ({provider, endpoint, history}) => {

    let subheader = "";
    if (endpoint.metadata && endpoint.metadata.description) {
        subheader = endpoint.metadata.description
    }

    let icon = <DoubleArrowIcon/>
    if (endpoint.metadata.role == "SINK") {
        icon = <DoubleArrowIcon style={{ transform: 'rotate(180deg)' }}/>
    }

    return (
        <ListItem
            button onClick={() => history.push("/endpoints/" + provider.key + "/" + endpoint.key)} disableRipple
            secondaryAction={
            <IconButton aria-label="detail" component={Link} to={"/endpoints/"  + provider.key + "/" + endpoint.key}>
                <NavigateNextIcon />
            </IconButton>
        }>
            <ListItemAvatar title={endpoint.metadata.role}>
                <Avatar>
                    {icon}
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={endpoint.metadata.name} secondary={subheader} />
        </ListItem>
    )
}

const EndpointList = (props) => {
    const {provider} = props
    return (
        <List>
            {
                provider.data &&
                provider.data.endpoints &&
                provider.data.endpoints
                    .sort((a,b) => a.metadata.name.localeCompare(b.metadata.name))
                    .map(endpoint => <EndpointListItem endpoint={endpoint} {...props} />)
            }
        </List>
    )
}

const ProviderMenu = (props) => {

    const {open, anchor, hideMenu} = props;

    const handleDelete = (e) => {
        e.stopPropagation();
    };

    const handleSettings = (e) => {
        e.stopPropagation();
    };

    const handleHideMenu = (e) => {
        e.stopPropagation();
        hideMenu()
    };

    return (
        <span>
            <Menu
                id="provider-menu"
                anchorEl={anchor}
                keepMounted
                open={open}
                onClose={handleHideMenu}>
                <MenuItem onClick={handleSettings}>Edit</MenuItem>
                <Divider />
                <MenuItem onClick={handleDelete}>Delete</MenuItem>
            </Menu>
        </span>
    )
}

const EndpointProvider = (props) => {

    const [menu, setMenu] = useState({
        open: false,
        anchor: null
    })

    const session = useSession()
    const provider = useQuery(['provider',props.match.params.key], fetchProvider(session.data, {
        enabled:!!session && !!session.data
    }));

    return (
        <>
            <SimpleProgress open={session.isLoading || provider.isLoading}/>
            <EndpointProviderHeader {...props}
                provider={provider}
                showMenu={anchor => setMenu({open:true, anchor:anchor})}
            />
            <EndpointList {...props}
                provider={provider}
            />
            <ProviderMenu {...props}
                open={menu.open}
                anchor={menu.anchor}
                hideMenu={() => setMenu({open:false, anchor:null})}
            />
        </>
    )
}

export default EndpointProvider