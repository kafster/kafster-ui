import {Links} from "../../util/links";
import axios from "axios";


export const uploadAsyncAPI = (session) => {
    return async (file) => {
        const form = new FormData();
        form.append("selectedFile", file);

        const links = new Links(session.links)
        return await axios.post(
            links.getHrefByRel("endpoints"),
            form,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(r => r.data)
    }
}

export const importAsyncAPI = (session) => {
    return async (file) => {
        const form = new FormData();
        form.append("selectedFile", file);

        const links = new Links(session.links)
        return await axios.post(
            links.getHrefByRel("endpoints"),
            form,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(r => r.data)
    }
}