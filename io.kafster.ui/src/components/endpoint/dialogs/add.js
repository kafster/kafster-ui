import {Grid} from "@mui/material";
import React from "react";
import TextField from "@mui/material/TextField";
import {SimpleDialog} from "../../util/dialog";

const AddEndpointsTitle = (props) => {
    return <span>Import AsyncAPI from a URL</span>
}

const AddEndpointsContent = (props) => {

    const {updateDialog, values} = props;

    const handleChange = (e) => {
        updateDialog(e.target.id, e.target.value)
    };

    return (
        <div>
            <form>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="location"
                            label="URL"
                            type="text"
                            //error={name?false:true}
                            fullWidth
                            disabled={false}
                            onChange={handleChange}
                            variant="standard"
                            placeholder={"The location of the AsyncAPI document"}
                            value={values["location"]||""}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="username"
                            label="Username"
                            type={"text"}
                            margin="normal"
                            onInput={handleChange}
                            variant="standard"
                            fullWidth
                            placeholder={"The optional user name to use when accessing the document"}
                            value={values["username"]||""}
                            autoComplete={"new-password"}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="password"
                            label="Password"
                            margin="normal"
                            onChange={handleChange}
                            variant="standard"
                            type="password"
                            fullWidth
                            placeholder={"The optional password to use to access the document"}
                            value={values["password"]||""}
                            autoComplete={"new-password"}
                        />
                    </Grid>
                </Grid>
            </form>

        </div>
    )
};

export const AddEndpointsDialog = (props) => {

    const {open, hideDialog, canSubmit, createEndpoints} = props;

    return (
        <SimpleDialog
            open={open}
            handleSubmit={createEndpoints}
            hideDialog={hideDialog}
            canSubmit={canSubmit}
            content={<AddEndpointsContent {...props}/>}
            title={<AddEndpointsTitle {...props}/>}
            {...props}/>
    )
}