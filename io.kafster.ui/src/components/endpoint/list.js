import Typography from "@mui/material/Typography";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Avatar,
    Button,
    Card,
    CardHeader,
    Container,
    Grid,
    LinearProgress, List, ListItem, ListItemAvatar, ListItemText
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import {styled} from "@mui/material/styles";
import {fetchProviders, importAsyncAPI, uploadAsyncAPI} from "./api";
import {useMutation, useQuery} from "react-query";
import {useSession} from "../dashboard/session";
import React, {useState} from "react";
import {AddEndpointsDialog} from "./dialogs/add";
import WorkspacesIcon from "@mui/icons-material/Workspaces";
import IconButton from "@mui/material/IconButton";
import {Link} from "react-router-dom";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Divider from "@mui/material/Divider";
import {KafkaLogo} from "../util/icon";
import {SimpleProgress} from "../util/progress";

const Input = styled('input')({
    display: 'none',
});

const EndpointListHeader = (props) => {

    const { providers, showDialog, uploadFile } = props

    const upload = (event) => {
        event.target.files && uploadFile(event.target.files[0])
    }

    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Endpoints</Typography>
                <Typography variant={"body"}>{providers.data ? providers.data.length : 0} Providers</Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" startIcon={<AddIcon/>} onClick={showDialog}>
                    Import AsyncAPI
                </Button>
            </Grid>
            <Grid item>
                <label htmlFor="contained-button-file">
                    <Input accept="*/json" id="contained-button-file" type="file" onChange={upload}/>
                    <Button variant="contained" startIcon={<ArrowUpwardIcon/>} component={"span"}>
                        Upload AsyncAPI
                    </Button>
                </label>
            </Grid>
        </Grid>
    )
}

const ProviderListItem = ({provider, history}) => {

    const endpoints = provider.endpoints

    let header = (
        <>
            <Typography>{provider.presentationName}</Typography>
            <Typography variant={"body2"}>{endpoints ? endpoints.length : 0} endpoints</Typography>
        </>
    )

    let subheader = "";
    if (provider.description) {
        subheader = provider.description
    }

    return (
    <>
        <ListItem
            button onClick={() => history.push("/providers/" + provider.key)} disableRipple
            secondaryAction={
            <IconButton aria-label="detail" component={Link} to={"/providers/"+provider.key}>
                <NavigateNextIcon />
            </IconButton>
        }>
            <ListItemAvatar>
                <Avatar>
                    <KafkaLogo/>
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={header} secondary={subheader} />
        </ListItem>
        <Divider/>
    </>
    )
}

const ProviderList = (props) => {
    const {providers} = props
    return (
        <Grid container spacing={1}>
                <List sx={{"width":"100%"}}>
                {
                    providers.data &&
                    providers.data
                        .sort((a,b) => a.presentationName.localeCompare(b.presentationName))
                        .map(provider => <ProviderListItem provider={provider} {...props} />)
                }
                </List>
        </Grid>
    )
}

const Endpoints = (props) => {

    const [dialog, setDialog] = useState({open:false,values:{}})

    const updateDialog = (field, value) => {
        const values = dialog.values
        values[field] = value
        setDialog({...dialog, values:values})
    }

    const session = useSession()

    const uploadMutation = useMutation(uploadAsyncAPI(session.data), {
        onSuccess: (data) => {
            props.history.push("/providers/" + data.key);
        }
    })

    const importMutation = useMutation(importAsyncAPI(session.data), {
        onSuccess: (data) => {
            props.history.push("/providers/" + data.key);
        }
    })

    const doSubmit = ()  => {
        importMutation.mutate(dialog.values)
    }

    const uploadFile = (file)  => {
        uploadMutation.mutate(file)
    }

    const providers = useQuery('providers', fetchProviders(session.data, {
        enabled:!!session && !!session.data
    }))

    return (
        <>
            <SimpleProgress open={session.isLoading || providers.isLoading}/>
            <EndpointListHeader
                upload={uploadFile}
                showDialog={() => setDialog({...dialog, open:true})}
                providers={providers}
                {...props}/>
            <ProviderList providers={providers} {...props}/>
            <AddEndpointsDialog
                open={dialog.open}
                hideDialog={() => setDialog({...dialog, open:false})}
                canSubmit={() => dialog.values["url"] || false}
                createSubscription={doSubmit}
                updateDialog={updateDialog}
                values={dialog.values}
                {...props}/>
        </>
    )
}

export default Endpoints