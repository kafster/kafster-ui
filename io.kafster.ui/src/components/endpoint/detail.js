import Typography from "@mui/material/Typography";
import {Grid, LinearProgress} from "@mui/material";
import {styled} from "@mui/material/styles";
import {fetchEndpoint} from "./api";
import {useQuery} from "react-query";
import {useSession} from "../dashboard/session";
import React from "react";

const Input = styled('input')({
    display: 'none',
});

const EndpointHeader = (props) => {

    const {endpoint} = props
    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Endpoints / {endpoint.provider.presentationName} / {endpoint.metadata.name}</Typography>
            </Grid>
        </Grid>
    )
}

const EndpointDetail = (props) => {

    const key = props.match.params.key
    const provider = props.match.params.provider

    const session = useSession()
    const endpoint = useQuery(['endpoint', provider, key], fetchEndpoint(session.data, {
        enabled:!!session && !!session.data
    }));

    if (session.isLoading || endpoint.isLoading) {
        return <LinearProgress color="secondary"/>
    }

    return (
        <>
            <EndpointHeader
                endpoint={endpoint.data}
                {...props}/>
        </>
    )
}

export default EndpointDetail