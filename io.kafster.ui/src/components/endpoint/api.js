import {Links} from "../util/links";
import axios from "axios";

export const fetchProviders = (session) => {
    return async () => {
        const links = new Links(session.links)
        return await axios.get(links.getHrefByRel("providers")).then(r => r.data.providers)
    }
}

export const fetchProvider = (session) => {
    return async ({ queryKey }) => {
        const links = new Links(session.templates)
        const template = links.getHrefTemplate("provider", {key: queryKey[1]})
        return await axios.get(template).then(r => r.data)
    }
}

export const fetchEndpoint = (session) => {
    return async ({ queryKey }) => {
        const links = new Links(session.templates)
        const template = links.getHrefTemplate("endpoint", {provider: queryKey[1], key: queryKey[2]})
        return await axios.get(template).then(r => r.data)
    }
}

export const uploadAsyncAPI = (session) => {
    return async (file, updateProgress) => {
        const form = new FormData();
        form.append("file", file);

        const links = new Links(session.links)
        return await axios.post(
            links.getHrefByRel("providers"),
            form,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                onUploadProgress: event => {
                    if (updateProgress) {
                        updateProgress(Math.round((event.loaded * 100) / event.total))
                    }
                }
            })
            .then(r => r.data)
    }
}

export const importAsyncAPI = (session) => {
    return async (data) => {
        const links = new Links(session.links)
        return await axios.post(links.getHrefByRel("providers"), data)
            .then(r => r.data)
    }
}