import {useQuery} from "react-query";
import axios from "axios";

const ROOT = "/services/session"

export const useSession = () => {

    return useQuery('session', () => {
        return axios.get(ROOT).then(r => r.data)
    });
}