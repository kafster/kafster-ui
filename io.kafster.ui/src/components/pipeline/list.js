import Typography from "@mui/material/Typography";
import {Avatar, Button, Grid, LinearProgress, List, ListItem, ListItemAvatar, ListItemText} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import React, {useState} from "react";
import {useSession} from "../dashboard/session";
import {useMutation, useQuery} from "react-query";
import {createPipeline, fetchPipelines} from "./api";
import {AddPipelineDialog} from "./dialogs/add";
import IconButton from "@mui/material/IconButton";
import {Link} from "react-router-dom";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import {KafkaLogo} from "../util/icon";
import Divider from "@mui/material/Divider";
import {SimpleProgress} from "../util/progress";

const PipelineListHeader = ({pipelines, showDialog}) => {
    const count = pipelines.data ? pipelines.data.pipelines.length : 0
    return (
        <Grid container justifyContent="flex-start" spacing={4}>
            <Grid item>
                <Typography variant={"h4"}>Pipelines</Typography>
                <Typography variant={"body"}>{count} Available</Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" startIcon={<AddIcon />} onClick={showDialog}>
                    Add Pipeline
                </Button>
            </Grid>
        </Grid>
    )
}

const PipelineListItem = ({pipeline, history}) => {

    const stages = pipeline.stages

    let header = (
        <>
            <Typography>{pipeline.name}</Typography>
            <Typography variant={"body2"}>{stages ? stages.length : 0} stages</Typography>
        </>
    )

    let subheader = "";
    if (pipeline.description) {
        subheader = pipeline.description
    }

    return (
        <>
            <ListItem
                button onClick={() => history.push("/pipeline/" + pipeline.key)} disableRipple
                secondaryAction={
                    <IconButton aria-label="detail" component={Link} to={"/pipeline/"+pipeline.key}>
                        <NavigateNextIcon />
                    </IconButton>
                }>
                <ListItemAvatar>
                    <Avatar>
                        <KafkaLogo/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={header} secondary={subheader} />
            </ListItem>
            <Divider/>
        </>
    )
}

const PipelineList = (props) => {
    const {pipelines} = props
    return (
        <Grid container spacing={1}>
            <List sx={{"width":"100%"}}>
                {
                    pipelines.data.pipelines &&
                    pipelines.data.pipelines
                        .sort((a,b) => a.name.localeCompare(b.name))
                        .map(pipeline => <PipelineListItem pipeline={pipeline} {...props} />)
                }
            </List>
        </Grid>
    )
}

const Pipelines = (props) => {

    const session = useSession()
    const [dialog, setDialog] = useState({open:false,values:{}})
    const mutation = useMutation(createPipeline(session.data), {
        onSuccess: (data) => {
            props.history.push("/pipelines/" + data.key);
        }
    })

    const updateDialog = (field, value) => {
        const values = dialog.values
        values[field] = value
        setDialog({...dialog, values:values})
    }

    const doSubmit = ()  => {
        mutation.mutate(dialog.values)
    }

    const pipelines = useQuery('pipelines', fetchPipelines(session.data), {
        enabled:!!session && !!session.data
    })

    return (
        <>
            <SimpleProgress open={session.isLoading || pipelines.isLoading}/>
            <PipelineListHeader
                showDialog={() => setDialog({...dialog, open:true})}
                pipelines={pipelines}
                {...props}/>
            { pipelines.isError && <div/>  }
            { pipelines.isSuccess && <PipelineList pipelines={pipelines} {...props}/> }
            <AddPipelineDialog
                open={dialog.open}
                hideDialog={() => setDialog({...dialog, open:false})}
                canSubmit={() => dialog.values["name"] || false}
                handleSubmit={doSubmit}
                updateAddDialog={updateDialog}
                values={dialog.values}
                {...props}/>
        </>
    )
}

export default Pipelines