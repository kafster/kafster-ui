import Typography from "@mui/material/Typography";
import {
    Avatar,
    Card,
    CardContent,
    CardHeader, Fab,
    LinearProgress,
    ListItem,
    ListItemAvatar, ListItemSecondaryAction,
    ListItemText, MenuItem
} from "@mui/material";
import {useSession} from "../dashboard/session";
import {useMutation, useQuery} from "react-query";
import {fetchSubscription} from "../subscription/api";
import React, {useState} from "react";
import {
    addStage,
    createPipeline,
    fetchPipeline,
    fetchPipelineStageProviders,
    setSource,
    setTarget,
    updateStage
} from "./api";
import IconButton from "@mui/material/IconButton";
import {TimestampMillis} from "../util/time";
import {List, Menu} from "@mui/icons-material";
import {Links} from "../util/links";
import Divider from "@mui/material/Divider";
import {AddStageDialog, EditStageDialog} from "./dialogs/stage";
import {SimpleProgress} from "../util/progress";
import {ConfigureSourceDialog} from "./dialogs/source";
import {ConfigureTargetDialog} from "./dialogs/target";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import CloseIcon from '@mui/icons-material/Close';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import AddIcon from '@mui/icons-material/Add';

const PipelineDescription = ({pipeline}) => {
    return pipeline.description && (
        <CardContent>
            <Typography variant="body1" color="textSecondary" component="p">
                {pipeline.description}
            </Typography>
        </CardContent>
    )
}

const PipelineSource = ({pipeline}) => {
    const source = pipeline.source;

    let type = "any";
    if ((source != null) && (source.type != null)) {
        type = source.type.name == null ? type : source.type.name;
    }
    return (
        <ListItem button disableRipple>
            <ListItemAvatar>
                <Avatar>
                    <DoubleArrowIcon style={{ transform: 'rotate(90deg)' }}/>
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Accepts" secondary={type} />
        </ListItem>
    )
};

const PipelineTarget = ({pipeline}) => {
    const target = pipeline.target;

    let type = "any";
    if ((target != null) && (target.type != null)) {
        type = target.type.name == null ? type : target.type.name;
    }

    return (
        <ListItem button disableRipple>
            <ListItemAvatar>
                <Avatar>
                    <DoubleArrowIcon style={{ transform: 'rotate(90deg)' }}/>
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Produces" secondary={type} />
        </ListItem>
    )
};

const PipelineStage = (props) => {

    const {showEditStageDialog, pipeline, index, stage} = props

    const upOk = index > 0
    const downOk = index < (pipeline.stages.length - 1)

    const links = new Links(stage.links)
    return (
        <ListItem button disableRipple onClick={e => showEditStageDialog(stage)}>
            <div className="MuiListItemAvatar-root">
                <div className="MuiAvatar-root MuiAvatar-circle MuiAvatar-colorDefault">
                    <img src={links.getHrefByRel("image")}/>
                </div>
            </div>
            <ListItemText primary={stage.presentationName} secondary={stage.description}/>
            <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="move up" disabled={!upOk}>
                    <ArrowUpwardIcon />
                </IconButton>
                <IconButton edge="end" aria-label="move down" disabled={!downOk}>
                    <ArrowDownwardIcon />
                </IconButton>
                <IconButton edge="end" aria-label="close" style={{"marginLeft":"8px"}}>
                    <CloseIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
};

const PipelineMenu = ({menu, hideMenu}) => {

    const handleDelete = (e) => {
        e.stopPropagation();
    };

    const handleShare = (e) => {
        e.stopPropagation();
    };

    const handleCopy = (e) => {
        e.stopPropagation();
    };

    const handleSettings = (e) => {
        e.stopPropagation();
    };

    const handleHideMenu = (e) => {
        e.stopPropagation();
        hideMenu();
    };

    return (
        <Menu
            id="pipeline-menu"
            anchorEl={menu.anchor}
            keepMounted
            open={menu.active}
            onClose={handleHideMenu}>
            <MenuItem onClick={handleSettings}>Settings</MenuItem>
            <MenuItem onClick={handleShare}>Share</MenuItem>
            <MenuItem onClick={handleCopy}>Copy</MenuItem>
            <Divider />
            <MenuItem onClick={handleDelete}>Delete</MenuItem>
        </Menu>
    )
};

const PipelineCard = (props) => {

    const { pipeline, showAddStageDialog, showMenu, history } = props

    return <Card raised={false}>
        <CardHeader
            avatar={
                <IconButton aria-label="detail" onClick={e=>history.goBack()}>
                    <ArrowBackIcon />
                </IconButton>
            }
            action={
                <span>
                    <IconButton aria-label="more" onClick={showMenu}>
                        <MoreVertIcon />
                    </IconButton>

                </span>
            }
            title={<Typography variant="h5" color="textPrimary">pipelines / {pipeline.name}</Typography>}
            subheader={<span><span>Updated </span><TimestampMillis timestamp={pipeline.modified}/></span>}
        />
        <PipelineDescription pipeline={pipeline}/>
        <CardContent>
            <Typography variant="h5" noWrap>
                <span>Stages</span>
                <Fab color="secondary" aria-label="add" size="small" style={{"margin":"8px 0 8px 8px"}} onClick={showAddStageDialog}>
                    <AddIcon />
                </Fab>
            </Typography>
            <List>
                <PipelineSource {...props}/>
                {
                    pipeline.stages &&
                    pipeline.stages
                        .map((stage, index) => <PipelineStage {...props} stage={stage} index={index}/>)
                }
                <PipelineTarget {...props}/>
            </List>
        </CardContent>
    </Card>
}

const Pipeline = (props) => {

    const session = useSession()
    const pipeline = useQuery(['pipeline', props.match.params.key], fetchPipeline(session.data), {
        enabled: !!session.data
    })
    const providers = useQuery(['pipeline.stage.providers'], fetchPipelineStageProviders(pipeline.data), {
        enabled: !!pipeline.data
    })

    const setSourceMutation = useMutation(setSource(pipeline.data))
    const setTargetMutation = useMutation(setTarget(pipeline.data))
    const addStageMutation = useMutation(addStage(pipeline.data))
    const updateStageMutation = useMutation(updateStage(pipeline.data))

    const [menu, setMenu] = useState({
        open: false,
        anchor: null
    })
    const [addStageDialog, setAddStageDialog] = useState({
        open:false,
        provider:null,
        configuration:{}
    })
    const [editStageDialog, setEditStageDialog] = useState({
        open:false,
        stage:null,
        provider:null,
        configuration:{}
    })
    const [sourceDialog, setSourceDialog] = useState({
        open:false,
        as:null,
        type:null
    })
    const [targetDialog, setTargetDialog] = useState({
        open:false,
        as:null,
        type:null
    })

    return (
        <>
            <SimpleProgress open={session.isLoading || pipeline.isLoading}/>
            <PipelineMenu {...props} open={menu.open}/>
            <PipelineCard {...props}
                  pipeline={pipeline.data}
                  showAddStageDialog={() => setAddStageDialog({...addStageDialog, open:true})}
                  showEditStageDialog={(stage) => setEditStageDialog({...editStageDialog, open:true, stage:stage})}
                  showSourceDialog={() => setSourceDialog({...sourceDialog, open:true})}
                  showTargetDialog={() => setTargetDialog({...targetDialog, open:true})}
                  showMenu = {(anchor) =>  setMenu({open:true,anchor:anchor})}
                  hideMenu = {() => setMenu({open:false,anchor:null})}
            />
            <AddStageDialog {...props}
                    open={addStageDialog.open}
                    providers={providers.data}
                    handleClose={() => setAddStageDialog({...addStageDialog, open:false})}
                    selectProvider={provider => setAddStageDialog({...addStageDialog, provider:provider})}
            />
            <EditStageDialog {...props}
                    open={editStageDialog.open}
                    handleClose={() => setEditStageDialog({...editStageDialog, open:false})}
            />
            <ConfigureSourceDialog {...props}
                    open={sourceDialog.open}
                    handleClose={() => setSourceDialog({...sourceDialog, open:false})}
            />
            <ConfigureTargetDialog {...props}
                   open={targetDialog.open}
                   handleClose={() => setTargetDialog({...targetDialog, open:false})}
            />
        </>
    )
}

export default Pipeline