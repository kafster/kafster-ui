import axios from "axios";
import {Links} from "../util/links";

export const fetchPipelines = (session) => {
    return async () => {
        const links = new Links(session.links)
        return await axios.get(links.getHrefByRel("pipelines")).then(r => r.data)
    }
}

export const fetchPipeline = (session) => {
    const links = new Links(session.templates)
    return async ({ queryKey }) => {
        const template = links.getHrefTemplate("pipeline", {key: queryKey[1]})
        return await axios.get(template).then(r => r.data)
    }
}

export const createPipeline = (session) => {
    return async (pipeline) => {
        const links = new Links(session.links)
        return await axios.post(links.getHrefByRel("pipelines"), pipeline).then(r => r.data)
    }
}

export const addStage = (pipeline) => {
    return async (stage) => {
        const links = new Links(pipeline.links)
        return await axios.post(links.getHrefByRel("stages"), stage).then(r => r.data)
    }
}

export const updateStage = (pipeline) => {
    return async (stage) => {
        const links = new Links(pipeline.links)
        return await axios.put(links.getHrefByRel("stages"), stage).then(r => r.data)
    }
}

export const setSource = (pipeline) => {
    return async (source) => {
        const links = new Links(pipeline.links)
        return await axios.put(links.getHrefByRel("source"), source).then(r => r.data)
    }
}

export const setTarget = (pipeline) => {
    return async (target) => {
        const links = new Links(pipeline.links)
        return await axios.put(links.getHrefByRel("target"), target).then(r => r.data)
    }
}

export const fetchPipelineStageProviders = (pipeline) => {
    return async () => {
        const links = new Links(pipeline.links)
        return await axios.get(links.getHrefByRel("providers")).then(r => r.data)
    }
}

