import React from "react";
import FormDialog from "../../util/form";
import TextField from "@mui/material/TextField";
import {SimpleDialog} from "../../util/dialog";

const AddPipelineTitle = (props) => {
    return <span>Add Pipeline</span>
}

const AddPipelineContent = (props) => {

    const {updateAddDialog} = props;

    const handleChange = (e) => {
        updateAddDialog(e.target.id, e.target.value)
    };

    return (
        <div>
            <TextField
                autoFocus
                required
                margin="dense"
                id="name"
                label="New Pipeline Name"
                type="text"
                //error={name?false:true}
                fullWidth
                disabled={false}
                onChange={handleChange}
                variant="standard"
            />
            <TextField
                id="description"
                label="Description"
                margin="dense"
                multiline
                rows={4}
                //value={description}
                fullWidth
                onChange={handleChange}
                variant="standard"
            />
        </div>
    )
};

export const AddPipelineDialog = (props) => {

    const {open, hideDialog, canSubmit, handleSubmit} = props;

    return (
        <SimpleDialog
            open={open}
            handleSubmit={handleSubmit}
            hideDialog={hideDialog}
            canSubmit={canSubmit}
            content={<AddPipelineContent {...props}/>}
            title={<AddPipelineTitle {...props}/>}
            {...props}/>
    )
}
