import React from "react";
import {Avatar, LinearProgress, ListItem, ListItemText} from "@mui/material";
import {List} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import {SimpleDialog} from "../../util/dialog";
import FormDialog from "../../util/form";
import Form from "@rjsf/core";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import StarIcon from '@mui/icons-material/Star';

const SelectProviderTitle = (props) => {
    return (
        <div>
            <Avatar aria-label="provider">
                <StarIcon/>
            </Avatar>
            <span style={{"marginLeft":"8px","marginTop":"12px"}}>Select a Stage Type</span>
        </div>
    )
};

const SelectProviderContent = (props) => {

    const { providers, selectProvider } = props;

    const ProviderListItem = ({provider}) => {
        return (
            <ListItem button onClick={() => selectProvider(provider)} disableRipple>
                <ListItemText primary={provider.name} secondary={provider.description}/>
            </ListItem>
        )
    }

    return (
        <List>
            { providers && providers.map(p => <ProviderListItem provider={p} {...props}/>)}
        </List>
    )
};

const ConfigureStageTitle = (props) => {
    const {selectStage, stage} = props;
    return (
        <div>
            <IconButton aria-label="back" onClick={(e=>selectStage(null))}>
                <ArrowBackIcon />
            </IconButton>
            <span style={{"marginLeft":"8px","marginTop":"12px"}}>Configure {stage.dialog.selected.name} Stage </span>
        </div>
    )
};

const ConfigureStageContent = (props) => {

    const {stage} = props;
    const selected = stage.dialog.selected;

    const options = {
        "ui:options": {
            label: false,
            title:""
        },
        "title":"",
        "ui:order": ["name", "description", "condition", "groupBy", "window", "*"],
        "mappings":{
            "ui:title":"Field Mappings"
        }
    };

    return (
        <ListItem>
            <Form schema={selected.model.schema} uiSchema={options} formData={selected.configuration} {...props}>
                <div>
                    <button type="submit" style={{display:"none"}}></button>
                </div>
            </Form>
        </ListItem>
    )
}

export const AddStageDialog = (props) => {

    const {provider, configuration, open, handleSubmit, setProvider, hideDialog} = props;

    return (
        <>
            <FormDialog
                open={open && provider}
                onSubmit={handleSubmit}
                hideDialog={hideDialog}
                canSubmit={() => true}
                title={<ConfigureStageTitle {...props}/>}
                schema={provider.model.schema}
                options={{}}
                initialValues={configuration}
                {...props}/>
            <SimpleDialog
                open={open && !provider}
                hideDialog={hideDialog}
                title={<SelectProviderTitle {...props}/>}
                content={<SelectProviderContent {...props}/>}
                handleSubmit={setProvider}
                canSubmit={() => !!provider}
                {...props}
            />
        </>
    )
}

export const EditStageDialog = (props) => {

    const {provider, configuration, open, handleSubmit, hideDialog} = props;

    return (
        <>
            <FormDialog
                open={open && provider}
                onSubmit={handleSubmit}
                hideDialog={hideDialog}
                canSubmit={() => true}
                title={<ConfigureStageTitle {...props}/>}
                schema={provider.model.schema}
                options={{}}
                initialValues={configuration}
                {...props}/>
        </>
    )
}


