import React from "react";
import Typography from "@mui/material/Typography";
import {Autocomplete} from "@mui/lab";
import {SimpleDialog} from "../../util/dialog";
import {TextField} from "@mui/material";


const Title = (props) => {
    return <Typography>Configure Pipeline Target</Typography>
}

const Content = (props) => {
    return (
        <div style={{ width: 300 }}>
            <Autocomplete
                {...props}
                id="pipeline-source"
                debug
                renderInput={(params) => <TextField {...params} label="debug" margin="normal" />}
            />
        </div>
    )
}

export const ConfigureTargetDialog = (props) => {

    const {open, handleSubmit, hideDialog} = props;

    return (
        <SimpleDialog
            open={open}
            handleSubmit={handleSubmit}
            hideDialog={hideDialog}
            canSubmit={() => true}
            content={<Content {...props}/>}
            title={<Title {...props}/>}
            {...props}/>
    )
};
