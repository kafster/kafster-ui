import './App.css';
import {createTheme} from "@mui/material";
import {indigo, blueGrey, grey} from "@mui/material/colors";
import {ThemeProvider} from "@emotion/react";
import {HashRouter as Router} from "react-router-dom";
import MiniDrawer from "./components/navigation/drawer";
import Application from "./components/navigation/application";
import {DefaultRoute,Route} from "./components/navigation/route";
import HomeIcon from '@mui/icons-material/Home';
import RadarIcon from '@mui/icons-material/Radar';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import BlurLinearIcon from '@mui/icons-material/BlurLinear';
import WorkspacesIcon from '@mui/icons-material/Workspaces';
import Home from "./components/dashboard/home";
import Subscriptions from "./components/subscription/list";
import Subscription from "./components/subscription/detail";
import Pipelines from "./components/pipeline/list";
import Pipeline from "./components/pipeline/detail";
import UserMenu from "./components/user/menu";
import ClusterAdmin from "./components/cluster/admin";
import Settings from "./components/admin/settings";
import { QueryClient, QueryClientProvider} from 'react-query';
import Views from "./components/view/list";
import Endpoints from "./components/endpoint/list";
import BarChartIcon from "@mui/icons-material/BarChart";
import React from "react";
import {faAngleDoubleDown} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import EndpointProvider from "./components/endpoint/provider";
import EndpointDetail from "./components/endpoint/detail";

const theme = createTheme({
  palette: {
    primary: {
      main:"#31708e"
    },
    secondary:{
      main:"#9e9e9e"
    }
  },
});

const queryClient = new QueryClient({
    defaultOptions: {
        stalePeriod: 5000
    }
});

const Actions = () => {
    return (
    <div  style={{marginLeft: 'auto'}}>
        <UserMenu/>
    </div>
    )
}

const App = () => {
  return (
      <QueryClientProvider client={queryClient}>
          <ThemeProvider theme={theme}>
              <Router>
                <Application action={<Actions/>}>
                    <Route name={"Home"} icon={<HomeIcon/>} path="/" component={Home}/>
                    <Route name={"Subscriptions"} icon={<AcUnitIcon/>} path="/subscriptions" component={Subscriptions} />
                    <Route name={"Pipelines"} icon={<BlurLinearIcon/>} path="/pipelines" component={Pipelines} />
                    <Route name={"Endpoints"} icon={<DoubleArrowIcon/>} path="/endpoints" component={Endpoints} />
                    <Route name={"Views"} icon={<BarChartIcon/>} path="/views" component={Views} />
                    <Route name={"Cluster"} icon={<WorkspacesIcon/>} path="/cluster" component={ClusterAdmin} />
                    <Route path="/subscriptions/:key" component={Subscription} />
                    <Route path="/pipelines/:key" component={Pipeline} />
                    <Route path="/providers/:key" component={EndpointProvider} />
                    <Route path="/endpoints/:provider/:key" component={EndpointDetail} />
                </Application>
              </Router>
          </ThemeProvider>
      </QueryClientProvider>
  )
};

export default App;
